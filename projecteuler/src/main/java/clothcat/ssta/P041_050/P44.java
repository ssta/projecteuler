package clothcat.ssta.P041_050;

import java.util.Arrays;

/**
 *
 */
public class P44 {

    public static void main(String[] args) {
        System.out.println(new P44().solve());
    }

    private int solve() {
        int[] pent = new int[10000];
        for (int i = 0; i < 10000; i++) {
            pent[i] = (i * (3 * i - 1)) / 2;
        }

        int diff = 0;
        for (int i = 1; i < pent.length; i++) {
            for (int j = 1; j < i; j++) {
                if (in(pent, pent[i] + pent[j])
                        && in(pent, pent[i] - pent[j])) {
                    diff=pent[i] - pent[j];
                    System.out.printf("Found: i=%d, j=%d, diff=%d\n", i, j, diff);
                }
            }
        }

        return diff;
    }

    boolean in(int[] a, int num) {
        return Arrays.binarySearch(a, num) >= 0;
    }
}
