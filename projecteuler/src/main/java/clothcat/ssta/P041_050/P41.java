package clothcat.ssta.P041_050;

import clothcat.ssta.lib.Primes;
import clothcat.ssta.lib.StringUtils;
import java.util.ArrayList;

/**
 *
 * We shall say that an n-digit number is pandigital if it makes use of all the
 * digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital and is
 * also prime.
 *
 * What is the largest n-digit pandigital prime that exists?
 *
 *
 */
public class P41 {

    public static void main(String[] args) {
        System.out.println(new P41().solve());
    }

    private long solve() {
        Primes primes = new Primes(123456789);
        ArrayList<Long> primeList = primes.getPrimeList();
        for (int i = primeList.size() - 1; i >= 0; i--) {
            if (StringUtils.isPandigital(String.valueOf(primeList.get(i)))) {
                return primeList.get(i);
            }
        }
        return 0;
    }
}
