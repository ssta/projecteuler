package clothcat.ssta.P041_050;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;

/**
 *
 */
public class P42 {

    public static void main(String[] args) {
        System.out.println(new P42().solve());
    }

    private int solve() {
        List<String> words = readWords();

        // generate the first few triangle numbers
        TreeSet<Integer> triangle = new TreeSet<Integer>();
        int t = 0;
        for (int i = 1; i < 20; i++) {
            t += i;
            triangle.add(t);
        }

        int count = 0;
        for (String s : words) {
            if (triangle.contains(scoreWord(s))) {
                count++;
            }
        }

        return count;
    }

    private List<String> readWords() {
        List<String> l = new ArrayList<String>();

        Scanner sc;

        sc = new Scanner(P42.class.getResourceAsStream("/P42_words.txt"));

        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            l.add(line);
        }
        return l;
    }

    private int scoreWord(String s) {
        int score = 0;
        for (char c : s.toCharArray()) {
            score += c - 'A' + 1;
        }
        return score;
    }
}
