package clothcat.ssta.P041_050;

import clothcat.ssta.lib.Integers;

/**
 *
 */
public class P45 {

    public static void main(String[] args) {
        System.out.println(new P45().solve());
    }

    private long solve() {
        int i = 1;
        long t = 0;
        while (true) {
            t += i;
            if (Integers.isPentagonal(t) && Integers.isHexagonal(t) && t > 40755) {
                break;
            }
        }
        return t;
    }
}
