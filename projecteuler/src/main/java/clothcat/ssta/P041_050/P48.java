package clothcat.ssta.P041_050;

import java.math.BigInteger;

/**
 *
 */
public class P48 {
    public static void main(String[] args) {
        System.out.println(new P48().solve());
    }

    private long solve() {
        BigInteger sum=BigInteger.ZERO;
        BigInteger m = BigInteger.valueOf(10000000000L);
        for(int i=1; i<=1000; i++){
            BigInteger b = BigInteger.valueOf(i).pow(i);
            
            sum=sum.add(b);
            sum=sum.mod(m);
        }
        
        long answer = sum.longValue();
        
        return answer;
    }
}
