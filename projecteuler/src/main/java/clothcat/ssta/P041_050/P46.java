package clothcat.ssta.P041_050;

import clothcat.ssta.lib.Primes;
import java.util.TreeSet;

/**
 *
 */
public class P46 {

    public static void main(String[] args) {
        System.out.println(new P46().solve());
    }

    private int solve() {
        TreeSet<Long> primes = new TreeSet<Long>(new Primes(10000).getPrimeList());

        // guess at upperbound and tweak later...
        final int BOUND = 10000;
        for (int i = 3; i < BOUND; i += 2) {
            int j = 0;
            boolean found = false;
            while (!found && 2 * j * j < BOUND) {
                if (primes.contains(Long.valueOf(i - (2 * j * j)))) {
                    found = true;
                }
                j++;
            }
            if (!found) {
                return i;
            }
        }
        // fallthrough - won't ever get here (unlesss the conjecture is true below BOUND), but the language requires it
        return 0;
    }
}
