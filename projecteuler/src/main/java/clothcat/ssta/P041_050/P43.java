package clothcat.ssta.P041_050;

/**
 *
 */
public class P43 {

    public static String show(int[] a) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < a.length; i++) {
            sb.append(a[i]);
        }
        return sb.toString();
    }

    public static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static boolean hasNext(int[] a) {
        int N = a.length;

        // find rightmost element a[k] that is smaller than element to its right
        int k;
        for (k = N - 2; k >= 0; k--) {
            if (a[k] < a[k + 1]) {
                break;
            }
        }
        if (k == -1) {
            return false;
        }

        // find rightmost element a[j] that is larger than a[k]
        int j = N - 1;
        while (a[k] > a[j]) {
            j--;
        }
        swap(a, j, k);

        for (int r = N - 1, s = k + 1; r > s; r--, s++) {
            swap(a, r, s);
        }

        return true;
    }

    public static void perm(int N) {

        // initialize permutation
        int[] a = new int[N];
        for (int i = 0; i < N; i++) {
            a[i] = i;
        }

        // print permutations
        int count = 1;
        long sum=0;
        while (count < 1000000 && hasNext(a)) {
            String s = show(a);
            long d234 = Long.parseLong(s.substring(1, 4));
            long d345 = Long.parseLong(s.substring(2, 5));
            long d456 = Long.parseLong(s.substring(3, 6));
            long d567 = Long.parseLong(s.substring(4, 7));
            long d678 = Long.parseLong(s.substring(5, 8));
            long d789 = Long.parseLong(s.substring(6, 9));
            long d8910 = Long.parseLong(s.substring(7, 10));
            if (d234 % 2 == 0 && d345 % 3 == 0 && d456%5==0 && d567%7==0 && d678%11==0 && d789%13==0 && d8910%17==0) {
                System.out.println("Found: "+s);
                sum+=Long.parseLong(s);
            }
        }
        System.out.println("Sum="+sum);

    }

    public static void main(String[] args) {
        perm(10);
    }
}
