package clothcat.ssta.P041_050;

import clothcat.ssta.lib.Primes;

/**
 *
 */
public class P47 {

    public static void main(String[] args) {
        System.out.println(new P47().solve());
    }

    private int solve() {
        final int BOUND = 150000;
        Primes p = new Primes(BOUND);
        for (int i = 3; i < BOUND; i++) {
            if (p.getDistinctPrimeFactors(i).size() == 4
                    && p.getDistinctPrimeFactors(i + 1).size() == 4
                    && p.getDistinctPrimeFactors(i + 2).size() == 4
                    && p.getDistinctPrimeFactors(i + 3).size() == 4) {
                return i;
            }
        }

        // fallthrough -- won't get here unless BOUND is too small
        return 0;
    }
}
