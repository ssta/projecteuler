package clothcat.ssta.P041_050;

import clothcat.ssta.lib.Primes;
import java.util.List;
import java.util.TreeSet;

/**
 *
 */
public class P50 {

    public static void main(String[] args) {
        System.out.println(new P50().solve());
    }

    private long solve() {
        final int MAX = 1000000;
        List<Long> p = new Primes(MAX).getPrimeList();
        TreeSet<Long> p1 = new TreeSet<Long>();
        for(Long l : p){
            p1.add(l);
        }
        long maxsum = 0;
        long maxcount = 0;

        for (int i = 0; i < p.size(); i++) {
            // stop once we hacve no chance of improvement
            if (maxsum > MAX - p.get(i)) {
                break;
            }
            long sum = 0;
            int count = 0;
            for (int j = i; sum < MAX && j < p.size(); j++) {
                sum += p.get(j);
                count++;
                if (p1.contains(sum)) {
                    if (count > maxcount) {
                        maxcount = count;
                        maxsum = sum;
                    }
                }
            }
        }
        return maxsum;
    }
}
