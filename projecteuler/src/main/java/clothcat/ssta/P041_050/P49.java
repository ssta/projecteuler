package clothcat.ssta.P041_050;

import clothcat.ssta.lib.Primes;
import clothcat.ssta.lib.StringUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 */
public class P49 {

    public static void main(String[] args) {
        System.out.println(new P49().solve());
    }

    private String solve() {
        String answer = "";
        ArrayList<Long> primeList = new Primes(9999).getPrimeList();
        HashMap<String, Set<Long>> perms = new HashMap<String, Set<Long>>();

        // find permutational primes...
        for (long l : primeList) {
            if (l > 999) {
                String key = StringUtils.sortWord("" + l);
                if (perms.containsKey(key)) {
                    perms.get(key).add(l);
                } else {
                    perms.put(key, new TreeSet<Long>());
                }
            }
        }

        // now list the ones with 3 perms...

        for (String s : perms.keySet()) {
            
            if (perms.get(s).size() >= 3) {
                Long[] a = perms.get(s).toArray(new Long[0]);
                if (a[1] - a[0] == a[2] - a[1]) {
                    answer = a[0].toString() + a[1].toString() + a[2].toString();
                }
            }
        }



        return answer;
    }
}
