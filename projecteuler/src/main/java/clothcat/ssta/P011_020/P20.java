package clothcat.ssta.P011_020;

import clothcat.ssta.lib.Integers;
import java.math.BigInteger;

/**
 *
 * n! means n × (n − 1) × ... × 3 × 2 × 1
 *
 * For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800, and the sum of the
 * digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
 *
 * Find the sum of the digits in the number 100!
 *
 *
 */
public class P20 {
    public static void main(String[] args) {
        System.out.println(new P20().solve());
    }

    private int solve() {
        // easy because Java has BigInteger...
        BigInteger i = BigInteger.ONE;
        BigInteger prod = BigInteger.ONE;
        while(i.intValue()<=100){
            prod=prod.multiply(i);
            i=i.add(BigInteger.ONE);
        }
        return Integers.sumDigits(prod).intValue();
    }
}
