package clothcat.ssta.P011_020;

/**
 *
 * If the numbers 1 to 5 are written out in words: one, two, three, four, five,
 * then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
 *
 * If all the numbers from 1 to 1000 (one thousand) inclusive were written out
 * in words, how many letters would be used?
 *
 * NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and
 * forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20
 * letters. The use of "and" when writing out numbers is in compliance with
 * British usage.
 *
 *
 */
public class P17 {

    public static void main(String[] args) {
        System.out.println(new P17().solve());
    }

    private int solve() {
        int count = 0;
        for (int i = 1; i <= 1000; i++) {
            String w = toWords(i);
            count += countLetters(w);

        }
        return count;
    }

    private int countLetters(String s) {
        int count = 0;
        for (char c : s.toCharArray()) {
            if (Character.isLetter(c)) {
                count++;
            }
        }
        return count;
    }

    private String toWords(int n) {
        // NOTE: Since we're not going to count spaces we can safely insert extra whitespace
        final String[] one_nineteen = new String[]{
            "", " one ", " two", " three ", " four ", " five", " six ", " seven ",
            " eight ", " nine ", " ten ", " eleven ", " twelve ", " thirteen ",
            " fourteen ", " fifteen ", " sixteen ", " seventeen ", " eighteen ",
            " nineteen "
        };
        final String[] tens = new String[]{
            "", " ten ", " twenty ", " thirty ", " forty ", " fifty ",
            " sixty ", " seventy ", " eighty ", " ninety "
        };
        if (n == 1000) {
            return " one thousand ";
        }
        if (n == 0) {
            return "";
        }
        String words = "";
        if (n >= 100) {
            int hundreds = n / 100;
            words = one_nineteen[hundreds] + " hundred ";
            n %= 100;
            if (n == 0) {
                return words;
            }
            words = words + " and ";
        }
        if (n > 19) {
            int t = n / 10;
            words = words + tens[t];
            n %= 10;
            if (n == 0) {
                return words;
            }
        }

        words = words + one_nineteen[n];
        return words;
    }
}
