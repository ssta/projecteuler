package clothcat.ssta.P011_020;

import clothcat.ssta.lib.Integers;
import java.math.BigInteger;

/**
 *
 */
public class P16 {

    public static void main(String[] args) {
        System.out.println(new P16().solve());
    }

    private BigInteger solve() {
        BigInteger two = new BigInteger("2");
        BigInteger n = two.pow(1000);
        BigInteger sumDigits = Integers.sumDigits(n);
        return sumDigits;
    }
}
