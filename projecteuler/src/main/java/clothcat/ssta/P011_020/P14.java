package clothcat.ssta.P011_020;

/**
 *
 */
public class P14 {
    public static void main(String[] args) {
        System.out.println(new P14().solve());
    }

    private String solve() {
        int maxlen = 0;
        int maxi = 0;
        for (int i = 1; i < 1000000; i++) {
            int len = collatzLength(i);
            if (len > maxlen) {
                maxlen = len;
                maxi = i;
            }
        }
        return "max len of: " + maxlen
                + " when i is: " + maxi;
    }

    private int collatzLength(long n) {
        // iteratively...stack couldn't handle the recurse depth...
        // even though recursing and memoizing feels cleaner...
        int count = 1;
        while (true) {
            if (n == 1) {
                return count;
            } else {
                n = (n % 2 == 0) ? n / 2 : (3 * n) + 1;
                count++;
            }
        }
    }
}
