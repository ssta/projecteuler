package clothcat.ssta.P011_020;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * You are given the following information, but you may prefer to do some
 * research for yourself.
 *
 * 1 Jan 1900 was a Monday. Thirty days has September, April, June and November.
 * All the rest have thirty-one, Saving February alone, Which has twenty-eight,
 * rain or shine. And on leap years, twenty-nine. A leap year occurs on any year
 * evenly divisible by 4, but not on a century unless it is divisible by 400.
 *
 * How many Sundays fell on the first of the month during the twentieth century
 * (1 Jan 1901 to 31 Dec 2000)?
 *
 *
 */
public class P19 {
    // Java date/calendar make this (relatively) easy to brute-force...
    public static void main(String[] args) {
        System.out.println(new P19().solve());
    }

    private int solve() {
        int count=0;
        
        for(int year=1901; year <=2000; year++){
            for(int month = 0; month <= Calendar.DECEMBER; month++){
                // use 3am to prevent any issues with BST
                Calendar c = new GregorianCalendar(year, month, 1, 3, 0);
                System.out.println("Trying: "+c.getTime().toGMTString());
                if(c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                    count++;
                }
            }
        }
        
        return count;
    }
}
