package clothcat.ssta.lib;

import java.util.Arrays;

/**
 * Various helper methods for manipulating Strings
 *
 */
public class StringUtils {

    /**
     * Returns true if the parameter String reads the same forwards as
     * backwards.
     *
     * Not the fastest method ever since it creates a stringbuilder rather than
     * just running through the array, but easy to write/read...
     */
    public static boolean isPalindrome(String s) {
        // guard against nullness
        if (s == null) {
            return false;
        }
        return s.equals(new StringBuilder(s).reverse().toString());
    }

    /**
     * Sorts the characters of s returning a new String with the characters
     * sorted
     */
    public static String sortWord(String s) {
        char[] c = s.toCharArray();
        Arrays.sort(c);
        return new String(c);
    }

    public static boolean isPandigital(String s) {
        switch (s.length()) {
            case 1:
                return "1".equals(sortWord(s));
            case 2:
                return "12".equals(sortWord(s));
            case 3:
                return "123".equals(sortWord(s));
            case 4:
                return "1234".equals(sortWord(s));
            case 5:
                return "12345".equals(sortWord(s));
            case 6:
                return "123456".equals(sortWord(s));
            case 7:
                return "1234567".equals(sortWord(s));
            case 8:
                return "12345678".equals(sortWord(s));
            case 9:
                return "123456789".equals(sortWord(s));

        }
        return false;
    }
}