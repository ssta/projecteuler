package clothcat.ssta.lib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/* This class cheats quite a bit by using the primes program from the 
 * bsdgames package (pretty ubiquitous on Linux) rather than implementing
 * a real sieve.  Efficient prime sieves are a bit of a bitch to write and since
 * I am a lazy sod, I am happy to just use the easy way out...
 *
 */
public class Primes {

    private ArrayList<Long> primes;

    /**
     * Generates primes up to maxPrime.
     *
     * Warning: This constructor blocks while the prime list is initialised!
     */
    public Primes(long maxPrime) {
        try {
            primes = new ArrayList<Long>();
            String[] command = new String[]{"primes", "2", String.valueOf(maxPrime)};
            ProcessBuilder pb = new ProcessBuilder(command);
            Process pr = pb.start();
            InputStream is = pr.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null) {
                primes.add(Long.valueOf(line));
            }
        } catch (IOException ex) {
            Logger.getLogger(Primes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Long> getPrimeFactors(long max) {
        long n = max;
        ArrayList<Long> factors = new ArrayList<Long>();
        ArrayList<Long> pr = getPrimeList();
        int i = 0;
        while (n > 1) {
            long testPrime = pr.get(i);
            if (n % testPrime == 0) {
                n /= testPrime;
                factors.add(testPrime);
            } else {
                // only move to the next prime if we didn't find a factor for the last
                i++;
            }
        }
        return factors;
    }

    public Set<Long> getDistinctPrimeFactors(long max) {
        Set<Long> factors = new HashSet<Long>();
        ArrayList<Long> primeFactors = getPrimeFactors(max);
        for (Long l : primeFactors) {
            factors.add(l);
        }
        return factors;
    }

    public ArrayList<Long> getPrimeList() {
        return primes;
    }

    /**
     * The number of divisors of a number depends on it's prime factors. A
     * composite p^a * q^b * r^c * ... has (a+1)(b+1)(c+1)... divisors.
     *
     * @return
     */
    public int countDivisors(long n) {
        List<Long> factors = getPrimeFactors(n);
        Map<Long, Integer> counts = new HashMap<Long, Integer>();
        for (Long l : factors) {
            if (counts.containsKey(l)) {
                counts.put(l, counts.get(l) + 1);
            } else {
                counts.put(l, 1);
            }
        }

        int count = 1;
        for (Long l : counts.keySet()) {
            count *= (counts.get(l) + 1);
        }

        return count;
    }
}
