package clothcat.ssta.lib;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Integers {

    public static BigInteger sumDigits(BigInteger n) {
        String s = n.toString();
        // unlikely we'd need a big integer for the sum, but technically 
        //possible and doesn't cost THAT much...
        BigInteger sum = BigInteger.ZERO;

        for (char c : s.toCharArray()) {
            sum = sum.add(BigInteger.valueOf(c - '0'));
        }
        return sum;
    }

    public static int sumDivisors(int n) {
        List<Integer> divisors = getProperDivisors(n);
        int sum = 0;
        for (int x : divisors) {
            sum += x;
        }
        return sum;
    }

    /**
     * inefficient way to do it, but easy to read/write/debug. May need to find
     * a better way with larger problem sizes...
     *
     * NOTE: "proper" means that n is NOT included as a divisor of n
     */
    public static List<Integer> getProperDivisors(int n) {
        List<Integer> l = new ArrayList<Integer>();
        for (int i = 1; i < n; i++) {
            if (n % i == 0) {
                l.add(i);
            }
        }
        return l;
    }

    /**
     * returns true is the sum of proper divisors of n is x and the sum of
     * proper divisors of x is n.
     *
     */
    public static boolean isAmicable(int n) {
        int x = sumDivisors(n);
        int sx = sumDivisors(x);
        return x != n && sx == n;
    }

    public static boolean isPerfectSquare(long n) {
        if (n < 0) {
            return false;
        }

        long tst = (long) (Math.sqrt(n) + 0.5);
        return tst * tst == n;
    }

    public static boolean isOdd(long n) {
        return n % 2 == 1;
    }

    public static boolean isTriangular(long n) {
        // n is triangular iff 8n+1 is odd and a perfect square
        // see http://en.wikipedia.org/wiki/Triangular_number#Triangular_roots_and_tests_for_triangular_numbers
        return isOdd((8 * n) + 1) && isPerfectSquare((8 * n) + 1);
    }

    public static boolean isPentagonal(long n) {
        // n is pentagonal iff:
        // 24x+1 is a perfect square and
        // sqrt(24x+1)==5 (mod 6)
        // see: http://en.wikipedia.org/wiki/Pentagonal_number#Tests_for_pentagonal_numbers
        long x = 24 * n + 1;
        return isPerfectSquare(x) && ((int) Math.sqrt(x)) % 6 == 5;
    }

    public static boolean isHexagonal(long n) {
        // n is hexagonal iff:
        // 8x+1 is a perfect square and
        // sqrt(8x+1)==3 (mod 4)
        long x = 8 * n + 1;
        return isPerfectSquare(x) && ((int) Math.sqrt(x)) % 4 == 3;
    }
}
