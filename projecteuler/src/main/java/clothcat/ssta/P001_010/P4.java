package clothcat.ssta.P001_010;

import clothcat.ssta.lib.StringUtils;

/**
 * A palindromic number reads the same both ways. The largest palindrome made
 * from the product of two 2-digit numbers is 9009 = 91 × 99.
 *
 * Find the largest palindrome made from the product of two 3-digit numbers.
 *
 */
public class P4 {

    public static void main(String[] args) {
        System.out.println(new P4().solve());
    }

    private int solve() {
        /* brute force but given the small nature of the problem that's fine...
         */
        int maxFound = 0;
        for (int a = 100; a < 1000; a++) {
            for (int b = 100; b < 1000; b++) {
                int c = a * b;
                if (StringUtils.isPalindrome(String.valueOf(c))) {
                    if (c > maxFound) {
                        maxFound = c;
                    }
                }
            }
        }
        return maxFound;
    }
}
