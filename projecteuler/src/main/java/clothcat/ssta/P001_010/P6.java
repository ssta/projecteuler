package clothcat.ssta.P001_010;

/**
 *
 * The sum of the squares of the first ten natural numbers is, 12 + 22 + ... +
 * 102 = 385
 *
 * The square of the sum of the first ten natural numbers is, (1 + 2 + ... +
 * 10)2 = 552 = 3025
 *
 * Hence the difference between the sum of the squares of the first ten natural
 * numbers and the square of the sum is 3025 − 385 = 2640.
 *
 * Find the difference between the sum of the squares of the first one hundred
 * natural numbers and the square of the sum.
 *
 *
 */
public class P6 {

    public static void main(String[] args) {
        System.out.println(new P6().solve());
    }

    private int solve() {
        return sqSum(100) - sumSq(100);
    }

    private int sumSq(int n) {
        return (n * (n + 1) * ((2 * n) + 1)) / 6;
    }

    private int sqSum(int n) {
        int s = tri(n);
        return s * s;
    }

    private int tri(int n) {
        return (n * (n + 1)) / 2;
    }
}
