package clothcat.ssta.P001_010;

import clothcat.ssta.lib.Primes;
import java.util.ArrayList;

/**
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 *
 * Find the sum of all the primes below two million.
 *
 */
public class P10 {

    public static void main(String[] args) {
        System.out.println(new P10().solve());
    }

    private long solve() {
        ArrayList<Long> primes = new Primes(2000000).getPrimeList();
        long sum = 0;
        for (Long l : primes) {
            sum += l;
        }
        return sum;
    }
}
