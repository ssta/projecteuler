package clothcat.ssta.P001_010;

import clothcat.ssta.lib.Primes;
import java.util.ArrayList;

/**
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
 * that the 6th prime is 13.
 *
 * What is the 10 001st prime number?
 *
 *
 */
public class P7 {
    public static void main(String[] args) {
        System.out.println(new P7().solve());
    }

    private long solve() {
        ArrayList<Long> list = new Primes(1000000).getPrimeList();
        return list.get(10000);
    }
}
