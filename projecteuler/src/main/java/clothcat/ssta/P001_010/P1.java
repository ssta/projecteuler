package clothcat.ssta.P001_010;

/**
 *
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we
 * get 3, 5, 6 and 9. The sum of these multiples is 23.
 *
 * Find the sum of all the multiples of 3 or 5 below 1000.
 *
 *
 */
public class P1 {

    /**
     * Find sum of multiples of 3 or 5 less than n
     */
    public int solve(int n) {
        int m = n - 1;
        int t3 = 3 * t(m / 3);
        int t5 = 5 * t(m / 5);
        int t15 = 15 * t(m / 15);

        return t3 + t5 - t15;
    }

    /**
     * Return the nth triangle number
     *
     * @param n
     */
    private int t(int n) {
        return (n * (n + 1)) / 2;
    }

    public static void main(String[] args) {
        int N = 1000;
        System.out.println(new P1().solve(N));
    }
}
