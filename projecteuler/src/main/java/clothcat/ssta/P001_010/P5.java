package clothcat.ssta.P001_010;

/**
 *
 *
 * 2520 is the smallest number that can be divided by each of the numbers from 1
 * to 10 without any remainder.
 *
 * What is the smallest positive number that is evenly divisible by all of the
 * numbers from 1 to 20?
 *
 */
public class P5 {

    public static void main(String[] args) {
        System.out.println(new P5().solve());
    }

    private long solve() {
        // can be done with pencil and paper.
        // we only need to worry about the maximum number of each of the prime factors
        // eg 16==2*2*2*2 so there are 2^4.
        // 2^4 * 3^2 * 5 * 7 * 11 * 13 * 17 * 19
        return 2 * 2 * 2 * 2 * 3 * 3 * 5 * 7 * 11 * 13 * 17 * 19;
    }
}
