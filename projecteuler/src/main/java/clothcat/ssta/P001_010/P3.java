package clothcat.ssta.P001_010;

/**
 * The prime factors of 13195 are 5, 7, 13 and 29.
 *
 * What is the largest prime factor of the number 600851475143 ?
 */
public class P3 {

    public static void main(String[] args) {
        System.out.println(new P3().solve());
    }

    private long solve() {
        // slow way to do it...eventually will need to write a fast prime sieve
        long n = 600851475143L;
        long maxPrime=0;
        for (int x = 2; n > 1; x++) {
            if (n % x == 0) {
                maxPrime = x;
                n /= x;
            }
        }
        return maxPrime;
    }
}
