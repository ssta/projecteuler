package clothcat.ssta.P021_030;

/**
 *
 */
public class P28 {

    public static void main(String[] args) {
        System.out.println(new P28().solve());
    }

    private int solve() {

        /*
         * We note that each n*n 'layer' of the 'spiral' adds: 4n^2-6n+6
         * to the total...n increasing by 2 each loop
         * 
         *   Add 1 for the 
         * solitary "1" in the centre of the spiral (when n=1)
         */
        int sum = 0;
        for (int n = 3; n <= 1001; n += 2) {
            sum = sum + (4 * n * n) - (6 * n) + 6;
        }
        return sum + 1;
    }
}
