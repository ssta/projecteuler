package clothcat.ssta.P021_030;

/**
 *
 */
public class P30 {

    public static void main(String[] args) {
        System.out.println(new P30().solve());
    }

    private int solve() {
        // need to be concerned with 6 digit numbers at most since 7 * 9^5 
        // is 413343 (nowhere near 7 digits).
        int sum = 0;
        for (int d1 = 0; d1 < 10; d1++) {
            for (int d2 = 0; d2 < 10; d2++) {
                for (int d3 = 0; d3 < 10; d3++) {
                    for (int d4 = 0; d4 < 10; d4++) {
                        for (int d5 = 0; d5 < 10; d5++) {
                            for (int d6 = 0; d6 < 10; d6++) {
                                int num = d1 * 100000 + d2 * 10000 + d3 * 1000 + d4 * 100 + d5 * 10 + d6;
                                int sumpow = (int) (Math.pow(d1, 5) + Math.pow(d2, 5) + Math.pow(d3, 5) + Math.pow(d4, 5) + Math.pow(d5, 5) + Math.pow(d6, 5));
                                if (num == sumpow && num>1) {
                                    System.out.println("Found: " + num);
                                    sum += num;
                                }
                            }
                        }
                    }
                }
            }
        }
        return sum;
    }
}
