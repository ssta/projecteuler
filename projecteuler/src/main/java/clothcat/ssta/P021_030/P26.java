package clothcat.ssta.P021_030;

/** an elegant method I found on the interweb somewhere when I couldn't get
 * my own cycle detection algorithm to be even close to quick.
 * (I think it was Kristian Edlund's site, but it was a fairly long while ago, 
 * so I might be misremembering)
 */
public class P26 {

    public static void main(String[] args) {
        System.out.println(new P26().solve());
    }
    
    private int solve() {
        int seqLen = 0;
        int num=0;
        for (int i = 1000; i > 1; i--) {
            if (seqLen >= i) {
                // if the max sequence length we have so far is bigger than i
                //then we can stop since the max sequence of remainders for 1/d 
                // is d-1 remainders.  This is why we start at the large end...
                break;
            }

            int[] remainders = new int[i];
            int val = 1;
            int pos = 0;
            while (remainders[val] == 0 && val != 0) {
                remainders[val] = pos;
                val *= 10;
                val %= i;
                pos++;
            }

            if (pos - remainders[val] > seqLen) {
                num=i;
                seqLen = pos - remainders[val];
            }
        }
        return num;
    }
}
