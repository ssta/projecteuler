package clothcat.ssta.P021_030;

/** Stole and adapted the permutation generator 
 * from: http://introcs.cs.princeton.edu/java/23recursion/PermutationsLex.java.html
 * 
 * It's fiddly to implement and this implementation is MUCH faster than the 
 * one I wrote at first! :)
 * 
 * The description of how it works is at:
 * http://en.wikipedia.org/wiki/Permutation#Generation_in_lexicographic_order
 *
 */
public class P24 {

    public static void show(int[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.printf("%d", a[i]);
        }
        System.out.printf("\n");
    }

    public static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static boolean hasNext(int[] a) {
        int N = a.length;

        // find rightmost element a[k] that is smaller than element to its right
        int k;
        for (k = N - 2; k >= 0; k--) {
            if (a[k] < a[k + 1]) {
                break;
            }
        }
        if (k == -1) {
            return false;
        }

        // find rightmost element a[j] that is larger than a[k]
        int j = N - 1;
        while (a[k] > a[j]) {
            j--;
        }
        swap(a, j, k);

        for (int r = N - 1, s = k + 1; r > s; r--, s++) {
            swap(a, r, s);
        }

        return true;
    }

    public static void perm(int N) {

        // initialize permutation
        int[] a = new int[N];
        for (int i = 0; i < N; i++) {
            a[i] = i;
        }

        // print permutations
        int count=1;
        while (count<1000000 && hasNext(a)) {
            count++;
        }
        show(a);
    }

    public static void main(String[] args) {
        int N = 1234;
        perm(10);
    }
}
