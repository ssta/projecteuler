package clothcat.ssta.P021_030;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * I preprocessed the file a little to make it a name per line. Not hard to do
 * in code, but trivial in shell and I'm lazy
 *
 */
public class P22 {

    public static void main(String[] args) {
        System.out.println(new P22().solve());
    }

    private int solve() {
        List<String> l = readNames();

        Collections.sort(l);
        int total = 0;
        for (int i = 0; i < l.size(); i++) {
            int score = worth(l.get(i)) * (i + 1);
            total += score;
        }

        return total;
    }

    private List<String> readNames() {
        List<String> l = new ArrayList<String>();

        Scanner sc;

        sc = new Scanner(P22.class.getResourceAsStream("/P22_names.txt"));

        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            l.add(line);
        }
        return l;
    }

    private int worth(String name) {
        name = name.toUpperCase();
        int sum = 0;
        for (char c : name.toCharArray()) {
            sum += c - 'A' + 1;
        }
        return sum;
    }
}
