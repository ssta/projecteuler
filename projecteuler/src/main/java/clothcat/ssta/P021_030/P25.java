package clothcat.ssta.P021_030;

import java.math.BigInteger;

/**
 *
 */
public class P25 {
    public static void main(String[] args) {
        System.out.println(new P25().solve());
    }

    private int solve() {
        BigInteger a = BigInteger.ONE;
        BigInteger b = BigInteger.ONE;
        BigInteger temp;
        int count=2;
        while(true){
            if(b.toString().length()>=1000){
                break;
            }
            temp=a.add(b);
            a=b;
            b=temp;
            count++;
        }
        
        return count;
    }
}
