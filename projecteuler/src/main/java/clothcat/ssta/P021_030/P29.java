package clothcat.ssta.P021_030;

import java.math.BigInteger;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 */
public class P29 {

    public static void main(String[] args) {
        System.out.println(new P29().solve());
    }

    private int solve() {
        SortedSet<BigInteger> set = new TreeSet<BigInteger>();
        for (int a = 2; a <= 100; a++) {
            for (int b = 2; b <= 100; b++) {
                BigInteger n = BigInteger.valueOf(a).pow(b);
                set.add(n);
            }
        }
        return set.size();
    }
}
