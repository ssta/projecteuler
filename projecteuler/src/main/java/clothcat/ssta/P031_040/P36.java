package clothcat.ssta.P031_040;

import clothcat.ssta.lib.StringUtils;

/**
 *
 */
public class P36 {

    public static void main(String[] args) {
        System.out.println(new P36().solve());
    }

    private int solve() {
        int sum = 0;
        for (int i = 0; i < 1000000; i++) {
            if (StringUtils.isPalindrome(Integer.toString(i)) && StringUtils.isPalindrome(Integer.toBinaryString(i))) {
                System.out.println("found: " + i);
                sum += i;
            }
        }
        return sum;
    }
}
