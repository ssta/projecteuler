package clothcat.ssta.P031_040;

import org.apache.commons.lang.math.Fraction;

/**
 *
 */
public class P33 {

    public static void main(String[] args) {
        System.out.println(new P33().solve());
    }

    private int solve() {
        Fraction prod = Fraction.ONE;
        /*The problem simplifies to looking for any of:
         * 1: ax/bx = a/b 
         * 2: ax/xb = a/b 
         * 3: xa/xb = a/b 
         * 4: xa/bx = a/b
         */
        for (int a = 1; a < 10; a++) {
            for (int b = 0; b < 10; b++) {
                for (int x = 0; x < 10; x++) {
                    int ln = 0;
                    int ld = 0;
                    int rn = 0;
                    int rd = 0;
                    // case 1
                    ln = (10 * a) + x;
                    ld = 10 * b + x;
                    rn = a;
                    rd = b;
                    if (compareEqual(ln, ld, rn, rd)) {
                        prod=prod.multiplyBy(Fraction.getFraction(ln, ld));
                        System.out.printf("found: %d/%d = %d/%d\n", ln, ld, rn, rd);
                    }
                    // case 2
                    ln = (10 * a) + x;
                    ld = 10 * x + b;
                    rn = a;
                    rd = b;
                    if (compareEqual(ln, ld, rn, rd)) {
                        prod=prod.multiplyBy(Fraction.getFraction(ln, ld));
                        System.out.printf("found: %d/%d = %d/%d\n", ln, ld, rn, rd);
                    }

                    // case 3
                    ln = (10 * x) + a;
                    ld = 10 * x + b;
                    rn = a;
                    rd = b;
                    if (compareEqual(ln, ld, rn, rd)) {
                        prod=prod.multiplyBy(Fraction.getFraction(ln, ld));
                        System.out.printf("found: %d/%d = %d/%d\n", ln, ld, rn, rd);
                    }

                    // case 4
                    ln = (10 * x) + a;
                    ld = 10 * b + x;
                    rn = a;
                    rd = b;
                    if (compareEqual(ln, ld, rn, rd)) {
                        prod=prod.multiplyBy(Fraction.getFraction(ln, ld));
                        System.out.printf("found: %d/%d = %d/%d\n", ln, ld, rn, rd);
                    }
                }
            }
        }
        System.out.println("Prod is: "+prod);
        return prod.getDenominator();
    }

    private boolean compareEqual(int ln, int ld, int rn, int rd) {
        // guard against /0 exceptions:
        if (ld == 0 || rd == 0) {
            return false;
        }
        if (ln >= ld || rn >= rd) {
            return false;
        }
        if (ln < 10) {
            return false;
        }
        if (ln % 10 == 0 || ld % 10 == 0) {
            return false;
        }
        Fraction f1 = Fraction.getFraction(ln, ld);
        Fraction f2 = Fraction.getFraction(rn, rd);
        return (f1.compareTo(f2) == 0);
    }
}
