package clothcat.ssta.P031_040;

import clothcat.ssta.lib.StringUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 */
public class P32 {

    public static void main(String[] args) {
        System.out.println(new P32().solve());
    }

    private int solve() {
        Set<Integer> prods = new TreeSet<Integer>();
        // ugly brute forceing
        // upper bounds for loops found through trial and error...
        for (int i = 2; i < 2000; i++) {
            for (int j = 1; j < 2000; j++) {
                int prod = i * j;
                if (prod > 987654321) {
                    break;
                }
                String s = "" + i + j + prod;
                if (StringUtils.isPandigital(s)) {
                    //System.out.printf("found: %d * %d = %d\n", i, j, prod);
                    prods.add(prod);
                }
            }
        }
        int result = 0;
        for (int i : prods) {
            result += i;
        }
        return result;
    }
}
