package clothcat.ssta.P031_040;

/**
 *
 */
public class P34 {

    int[] digFact = new int[]{
        1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880
    };

    public static void main(String[] args) {
        System.out.println(new P34().solve());
    }

    private int solve() {
        int sum = 0;
        // upper bound (found by experimentation)...there are only 4 of these 
        // numbers, the biggest is 40585 (in fact, there are only 2 since 1 and
        // 2 don't count!)
        for (int i = 3; i <= 40585; i++) {
            if (test(i)) {
                System.out.println("found: " + i);
                sum += i;
            }
        }
        return sum;
    }

    private boolean test(int i) {
        int sum = 0;
        for (char c : String.valueOf(i).toCharArray()) {
            sum += digFact[c - '0'];
        }
        return sum == i;
    }
}
