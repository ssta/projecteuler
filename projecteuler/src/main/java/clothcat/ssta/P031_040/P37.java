package clothcat.ssta.P031_040;

import clothcat.ssta.lib.Primes;
import java.util.List;
import java.util.TreeSet;

/**
 *
 */
public class P37 {

    TreeSet<Long> primes;

    public static void main(String[] args) {
        System.out.println(new P37().solve());
    }

    public boolean isTruncatablePrime(long n) {
        /* Using integer ops (division and mod) rather than String ops would be
         * a little faster.  TODO: rewrite for the efficiency gain
         */
        if (!isPrime(n)) {
            return false;
        }
        for (int i = 1; i < String.valueOf(n).length(); i++) {
            if (!isPrime(Long.parseLong(String.valueOf(n).substring(i)))) {
                return false;
            }
            if (!isPrime(Long.parseLong(String.valueOf(n).substring(0, i)))) {
                return false;
            }
        }
        return true;
    }

    private int solve() {
        int sum = 0;
        List<Long> p = new Primes(1000000).getPrimeList();
        primes = new TreeSet<Long>();
        // only need primes with all digits 1,3,7 or 9
        for (long l : p) {
            primes.add(l);
        }

        for (long l : primes) {
            if (isTruncatablePrime(l) && l > 10) {
                System.out.println("Found: " + l);
                sum += l;
            }
        }
        return sum;
    }

    private boolean isPrime(long number) {
        return primes.contains(number);
    }
}
