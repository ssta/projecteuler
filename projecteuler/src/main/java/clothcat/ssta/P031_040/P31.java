package clothcat.ssta.P031_040;

/**
 *
 */
public class P31 {
    /*-1 placeholder to make indexing nicer*/

    int[] coins = new int[]{-1, 1, 2, 5, 10, 20, 50, 100, 200};

    public static void main(String[] args) {
        System.out.println(new P31().solve());
    }

    private int solve() {
        int amount = 200;
        /* Nice elegant iterative method I stoled from teh forums..
         * Much nicer than the recursive method I had originally
         */
        int count = 0;
        int a, b, c, d, e, f, g;
        for (a = amount; a >= 0; a -= 200) {
            for (b = a; b >= 0; b -= 100) {
                for (c = b; c >= 0; c -= 50) {
                    for (d = c; d >= 0; d -= 20) {
                        for (e = d; e >= 0; e -= 10) {
                            for (f = e; f >= 0; f -= 5) {
                                for (g = f; g >= 0; g -= 2) {
                                    count++;
                                }
                            }
                        }
                    }
                }
            }
        }
        return count;
    }
}
