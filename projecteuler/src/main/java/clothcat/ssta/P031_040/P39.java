package clothcat.ssta.P031_040;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * If p is the perimeter of a right angle triangle with integral length sides,
 * {a,b,c}, there are exactly three solutions for p = 120.
 *
 * {20,48,52}, {24,45,51}, {30,40,50}
 *
 * For which value of p ≤ 1000, is the number of solutions maximised?
 *
 *
 */
public class P39 {

    public static void main(String[] args) {
        System.out.println(new P39().solve());
    }

    private int solve() {
        Map<Integer, Integer> counts = new HashMap<Integer, Integer>();

        // a,b < 500, so we can just brute force it as 500 is small...
        for (int a = 1; a < 500; a++) {
            for (int b = 1; b < a; b++) {
                int c2 = a * a + b * b;
                int c = (int) Math.sqrt(c2);
                if (c2 == (c * c)) {
                    int perim = a + b + c;

                    if (counts.containsKey(perim)) {
                        counts.put(perim, counts.get(perim) + 1);
                    } else {
                        counts.put(perim, 1);
                    }
                }
            }
        }

        int maxcount = 0;
        int maxperim = 0;
        for (Integer i : counts.keySet()) {
            if (counts.get(i) > maxcount) {
                maxcount = counts.get(i);
                maxperim = i;
            }
        }

        return maxperim;
    }
}
