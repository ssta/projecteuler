package clothcat.ssta.P031_040;

import clothcat.ssta.lib.Integers;
import clothcat.ssta.lib.StringUtils;

/**
 *
 */
public class P38 {

    public static void main(String[] args) {
        System.out.println(new P38().solve());
    }

    private String solve() {
        /* we need: 
         *   xxxx * 1 = xxxx
         *   xxxx * 2 = xxxxx
         * 
         */
        String x = "";
        for (int i = 1234; i < 10000; i++) {
            String t = "" + i + "" + (2 * i);
            if (StringUtils.isPandigital(t)) {
                x = t;
            }
        }
        return x;
    }
}
