package clothcat.ssta.P031_040;

import clothcat.ssta.lib.Primes;
import java.util.TreeSet;

/**
 *
 */
public class P35 {

    public static void main(String[] args) {
        System.out.println(new P35().solve());
    }

    private int solve() {
        // start counting at 2 because we filtered out 2 and 5 which are valid examples
        int count = 2;
        TreeSet<Long> primes = new TreeSet<Long>(new Primes(1000000).getPrimeList());
        TreeSet<Long> filtered = new TreeSet<Long>();
        // remove impossible primes
        for (long l : primes) {
            if (!filter(l)) {
                filtered.add(l);
            }
        }

        for (long l : filtered) {
            int len = String.valueOf(l).length();
            boolean isCircularPrime = true;
            for (int i = 0; i < len; i++) {
                // rotate right
                l = rotRight(l, len);
                if (!filtered.contains(l)) {
                    isCircularPrime = false;
                }
            }
            if (isCircularPrime) {
                count++;
                System.out.printf("Found: %d; %d\n", count, l);
            }
        }

        return count;
    }

    private long rotRight(long l, int len) {
        return (long) (l / 10 + ((l % 10) * Math.pow(10, len - 1)));
    }

    private boolean filter(long l) {
        String s = String.valueOf(l);
        return (s.contains("0") || s.contains("2") || s.contains("4") || s.contains("5")
                || s.contains("6") || s.contains("8"));
    }
}
