package clothcat.ssta.P031_040;

/**
 *
 * An irrational decimal fraction is created by concatenating the positive
 * integers:
 *
 * 0.123456789101112131415161718192021...
 *
 * It can be seen that the 12th digit of the fractional part is 1.
 *
 * If dn represents the nth digit of the fractional part, find the value of the
 * following expression.
 *
 * d1 × d10 × d100 × d1000 × d10000 × d100000 × d1000000
 *
 *
 */
public class P40 {

    public static void main(String[] args) {
        System.out.println(new P40().solve());
    }

    private int solve() {
        int digit = 0;
        int prod = 1;
        for (int i = 1; digit <= 1000000; i++) {
            for (char c : String.valueOf(i).toCharArray()) {
                digit++;
                if (digit == 10 || digit == 100 || digit == 1000 || digit == 10000 || digit == 100000 || digit == 1000000) {
                    prod*=c-'0';
                }
            }
        }

        return prod;
    }
}
