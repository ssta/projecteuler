package clothcat.ssta.lib;

import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;

/**
 *
 * @author ssta
 */
public class StringUtilsTest extends TestCase {

    public StringUtilsTest(String testName) {
        super(testName);
    }

    /**
     * Test of isPalindrome method, of class StringUtils.
     */
    public void testIsPalindrome() {
        System.out.println("isPalindrome");
        assertEquals(true, StringUtils.isPalindrome(""));
        assertEquals(true, StringUtils.isPalindrome("a"));
        assertEquals(true, StringUtils.isPalindrome("aa"));
        assertEquals(true, StringUtils.isPalindrome("aba"));
        assertEquals(true, StringUtils.isPalindrome("abba"));
        assertEquals(false, StringUtils.isPalindrome("ab"));
        assertEquals(false, StringUtils.isPalindrome("abb"));
        assertEquals(false, StringUtils.isPalindrome("baba"));
        assertEquals(false, StringUtils.isPalindrome(null));
    }
}
